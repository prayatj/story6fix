from django.test import TestCase, Client
from django.urls import resolve
from .models import Post

class Posting_unit_test(TestCase):
    def test_posting_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_posting_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_posting_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_posting_can_create_models(self):
        new_post = Post.objects.create(content="im fine")
        count = Post.objects.all().count()
        self.assertEqual(count, 1)
    
    


# Create your tests here.
